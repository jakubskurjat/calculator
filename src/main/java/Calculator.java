public class Calculator {
    public static int addition(int a, int b) {
        return a + b;
    }

    public static int subtraction(int minuend, int subtrahend) {
        return minuend - subtrahend;
    }

    public static int multiplication(int a, int b) {
        return a * b;
    }

    public static int division(int dividend, int divisor) {
        try {
            return dividend / divisor;
        } catch (ArithmeticException exception) {
            throw new ArithmeticException("Cannot be divided by 0!");
        }
    }

    public static int power(int a, int power) {
        return (int) Math.pow(a, power);
    }
}
