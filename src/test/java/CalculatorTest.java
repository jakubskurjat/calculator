import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class CalculatorTest {

    @Test
    void testAddition() {
        //Arrange
        int a = 10;
        int b = 6;

        //Act
        int actual = Calculator.addition(a, b);

        //Assert
        assertEquals(16, actual);
    }

    @Test
    void testSubtraction() {
        //Arrange
        int a = 19;
        int b = 7;

        //Act
        int actual = Calculator.subtraction(a, b);
        int actual2 = Calculator.subtraction(b, a);

        //Assert
        assertEquals(12, actual);
        assertEquals(-12, actual2);
    }

    @Test
    void testMultiplication() {
        //Arrange
        int a = 9;
        int b = 6;

        //Act
        int actual = Calculator.multiplication(a, b);

        //Assert
        assertEquals(54, actual);
    }

    @Test
    void testDivision() {
        //Arrange
        int a = 28;
        int b = 7;

        //Act
        int actual = Calculator.division(a, b);
        int actual2 = Calculator.division(b, a);

        //Assert
        assertEquals(4, actual);
        assertEquals(0, actual2);
    }

    @Test
    void calculator_When_DividesByZero_ShouldShowAMessageAboutIt() {
        //Arrange
        int a = 10;
        int b = 0;

        //Act
        Throwable throwable = assertThrows(ArithmeticException.class, new Executable() {
            @Override
            public void execute() {
                Calculator.division(a, b);
            }
        });

        //Assert
        assertEquals("Cannot be divided by 0!", throwable.getMessage());
    }

    @Test
    void testPower() {
        //Arrange
        int a = 3;
        int b = 5;

        //Act
        int actual = Calculator.power(a, b);
        int actual2 = Calculator.power(b, a);

        //Assert
        assertEquals(243, actual);
        assertEquals(125, actual2);
    }
}